CREATE TABLE `sequence_number_generator` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`warehouse_id` BIGINT(20) NOT NULL,
	`prefix` VARCHAR(50) NOT NULL,
	`suffix` BIGINT(20) NOT NULL,
	`length` BIGINT(20) NOT NULL,
	`entity_type` VARCHAR (20) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;