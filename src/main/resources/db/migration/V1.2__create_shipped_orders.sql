CREATE TABLE `shipped_order` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`lock_version` BIGINT(20) NULL DEFAULT 0,
	`order_number` VARCHAR(20) NOT NULL,
	`freight_charges` DECIMAL(10,2) NULL DEFAULT NULL,
	`shipped_date` DATE NULL DEFAULT NULL,
	`is_invoiced` TINYINT(1) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `shipped_order_number_fk` (`order_number`),
	CONSTRAINT `shipped_order_number_fk` FOREIGN KEY (`order_number`) REFERENCES `orders` (`ordernumber`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `shipped_order_line_item` (
	`ID` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`shipped_order_id` BIGINT(20) NULL DEFAULT NULL,
	`product_code` VARCHAR(20) NOT NULL,
	`quantity_shipped` INT(11) NOT NULL,
	PRIMARY KEY (`ID`),
	INDEX `shipped_order_fk` (`shipped_order_id`),
	CONSTRAINT `shipped_order_fk` FOREIGN KEY (`shipped_order_id`) REFERENCES `shipped_order` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;