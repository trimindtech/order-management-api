CREATE TABLE `sales_quotation` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`lock_version` BIGINT(20) NULL DEFAULT 0,
	`quote_number` VARCHAR(20) NOT NULL,
	`created_by` VARCHAR(20) NOT NULL,
	`customer_id` VARCHAR(20) NOT NULL,
	`notes` VARCHAR(256) NOT NULL,
	`creation_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`is_sales_order_created` TINYINT(1) NOT NULL DEFAULT 0,
	PRIMARY KEY (`id`),
	INDEX `sales_quote_quote_number_fk` (`quote_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `sales_quotation_line_item` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`sales_quotation_id` BIGINT(20) NOT NULL,
	`product_code` VARCHAR(20) NOT NULL,
	`product_description` VARCHAR(256) NOT NULL,
	`uom` VARCHAR(256) NOT NULL,
	`quantity` INT(11) NOT NULL,
	`list_price` DECIMAL (7, 2) NOT NULL,
	`offered_price` DECIMAL (7, 2) NOT NULL,
	`tax` DECIMAL (7, 2) NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `sales_quotation_fk` (`sales_quotation_id`),
	CONSTRAINT `sales_quotation_fk` FOREIGN KEY (`sales_quotation_id`) REFERENCES `sales_quotation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;