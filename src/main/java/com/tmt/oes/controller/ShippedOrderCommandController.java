package com.tmt.oes.controller;

import com.tmt.core.commands.controller.AbstractBaseEntityCommandController;
import com.tmt.oes.model.ShippedOrder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/shipped-order")
public class ShippedOrderCommandController extends AbstractBaseEntityCommandController<ShippedOrder, Long> {
}
