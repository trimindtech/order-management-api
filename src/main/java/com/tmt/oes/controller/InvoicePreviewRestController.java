package com.tmt.oes.controller;

import com.tmt.oes.invoice.model.sharedobjects.GenerateInvoicesPreviewRequest;
import com.tmt.oes.model.dtos.InvoiceDTO;
import com.tmt.oes.service.InvoicePreviewService;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("invoices-preview")
@AllArgsConstructor
public class InvoicePreviewRestController {

    private final InvoicePreviewService invoicePreviewService;

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    List<InvoiceDTO> generateInvoicePreview(@RequestBody @Valid final GenerateInvoicesPreviewRequest request) {
        validatePreviewRequest(request);
        String customerId = request.getCustomerId();
        LocalDate cutOffDate = request.getCutOffDate();
        if(cutOffDate == null) {
            cutOffDate = LocalDate.now().plusDays(1);
        }

        if(StringUtils.isNotEmpty(customerId)) {
           return invoicePreviewService.generatePreviewForCustomer(customerId, cutOffDate, request.getUserId());
        } else if(StringUtils.isNotEmpty(request.getOrderId())) {
            return invoicePreviewService.generatePreviewForOrder(request.getOrderId(), request.getUserId());
        } else {
            return invoicePreviewService.generatePreviewForOrdersShippedBeforeDate(cutOffDate, request.getUserId());
        }
    }

    private void validatePreviewRequest(final GenerateInvoicesPreviewRequest request) {
        //TODO: Implement this
    }
}
