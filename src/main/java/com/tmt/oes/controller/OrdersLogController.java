package com.tmt.oes.controller;

import com.tmt.core.query.AbstractBaseEntityQueryController;
import com.tmt.oes.model.OrdersLog;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/orders")
@Api(value="Orders Log", description="Operations pertaining to Orders Created")
public class OrdersLogController extends AbstractBaseEntityQueryController<OrdersLog, Long> {
}
