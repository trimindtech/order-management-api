package com.tmt.oes.controller;

import com.tmt.core.query.AbstractBaseEntityQueryController;
import com.tmt.oes.model.SalesQuotation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("sales-quotation")
public class SalesQuotationQueryController extends AbstractBaseEntityQueryController<SalesQuotation, Long> {
}
