package com.tmt.oes.controller;

import com.tmt.core.query.AbstractBaseSharedObjectQueryController;
import com.tmt.oes.model.SalesOrder;
import com.tmt.oes.model.dtos.SalesOrderDTO;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("sales-orders")
public class SalesOrderQueryController extends AbstractBaseSharedObjectQueryController<SalesOrder, String, SalesOrderDTO> {
}
