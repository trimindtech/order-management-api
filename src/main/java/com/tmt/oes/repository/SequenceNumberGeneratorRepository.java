package com.tmt.oes.repository;

import com.tmt.oes.model.SequenceNumberGenerator;
import com.tmt.oes.model.enums.SequenceEntityType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SequenceNumberGeneratorRepository extends JpaRepository<SequenceNumberGenerator, Long>{
    SequenceNumberGenerator findOneByWarehouseIdAndEntityType(Long warehouseId, SequenceEntityType entityType);
}
