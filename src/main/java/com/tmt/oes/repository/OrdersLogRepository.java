package com.tmt.oes.repository;

import com.tmt.oes.model.OrdersLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


@Repository
public interface OrdersLogRepository extends JpaRepository<OrdersLog, Long>, JpaSpecificationExecutor<OrdersLog> {

}

