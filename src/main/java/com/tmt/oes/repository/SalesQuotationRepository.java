package com.tmt.oes.repository;

import com.tmt.oes.model.SalesQuotation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SalesQuotationRepository extends JpaRepository<SalesQuotation, Long>, JpaSpecificationExecutor<SalesQuotation> {
}
