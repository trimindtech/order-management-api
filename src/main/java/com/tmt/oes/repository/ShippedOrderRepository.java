package com.tmt.oes.repository;

import com.tmt.oes.model.SalesOrder;
import com.tmt.oes.model.ShippedOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.time.LocalDate;
import java.util.List;

public interface ShippedOrderRepository extends JpaRepository<ShippedOrder, Long>, JpaSpecificationExecutor<ShippedOrder> {
    List<ShippedOrder> findBySalesOrder(SalesOrder salesOrder);

    List<ShippedOrder> findByShippedDateLessThanEqualAndIsInvoiced(LocalDate cutOffDate, boolean isInvoiced);

    List<ShippedOrder> findBySalesOrderCustomerIdAndIsInvoicedAndShippedDateLessThanEqual(String customerId, boolean isInvoiced, LocalDate cutOffdate);

    List<ShippedOrder> findBySalesOrderAndIsInvoiced(SalesOrder salesOrder, boolean isInvoiced);
}
