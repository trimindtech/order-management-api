package com.tmt.oes.model.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
public class InvoiceDTO {
    private BigDecimal amountPaid;
    private String invoiceNumber;
    private String batchNumber;
    private String invoicedBy;
    private BigDecimal discount;
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(example = "2016-01-01")
    private LocalDate invoiceDate;
    private BigDecimal invoiceAmount; //amount without tax and other charges
    private String orderNumber;
    private BigDecimal taxAmount;
    private String customerId;
    private BigDecimal freightCharges;
    private BigDecimal miscellaneousCharges;
    private Long shipmentId;
    private List<InvoiceItemDTO> invoiceItemDTOS;
}