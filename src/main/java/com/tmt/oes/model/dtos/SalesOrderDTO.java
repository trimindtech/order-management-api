package com.tmt.oes.model.dtos;

import com.tmt.core.base.BaseReadonlySharedObject;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class SalesOrderDTO implements BaseReadonlySharedObject<String> {
    private String id;
    private String orderNumber;
    private LocalDate orderDate;
    private String customerId;
    private Long jobSiteId;
    private Long aging;
    private BigDecimal totalAmount;
    private BigDecimal amountPaid;
    private String orderStatus;
    private String invoiceStatus;
    private String shippingStatus;
    private String paymentStatus;
    private BigDecimal tax;
    private String saleType;
}
