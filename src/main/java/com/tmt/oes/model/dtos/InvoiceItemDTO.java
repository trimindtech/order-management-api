package com.tmt.oes.model.dtos;


import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class InvoiceItemDTO {
    private String productCode;
    private BigDecimal price;
    private Integer quantity;
    private BigDecimal taxAmount;
}
