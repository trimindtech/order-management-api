package com.tmt.oes.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@ToString(exclude = "shippedOrder")
@EqualsAndHashCode(exclude="shippedOrder")
public class ShippedOrderLineItem {

    @Id
    @GeneratedValue
    private Long id;
    private String productCode;
    private Integer quantityShipped;
    @ManyToOne
    @JoinColumn(name = "shipped_order_id")
    @JsonBackReference
    private ShippedOrder shippedOrder;
}
