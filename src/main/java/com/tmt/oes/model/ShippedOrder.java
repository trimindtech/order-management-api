package com.tmt.oes.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.tmt.core.base.BaseEntity;
import com.tmt.core.base.BaseReadonlyEntity;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(of="id")
public class ShippedOrder implements BaseEntity<Long> {

    @Id
    @GeneratedValue
    private Long id;
    private Long lockVersion;
    @ManyToOne
    @JoinColumn(name = "order_number")
    private SalesOrder salesOrder;
    private BigDecimal freightCharges;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate shippedDate;
    @Column(name = "is_invoiced")
    private Boolean isInvoiced;
    @OneToMany(mappedBy = "shippedOrder", cascade = CascadeType.ALL)
    @Setter(AccessLevel.NONE)
    @JsonManagedReference
    private List<ShippedOrderLineItem> orderLineItems;

    public void setOrderLineItems(List<ShippedOrderLineItem> orderLineItems) {
        this.orderLineItems = orderLineItems;
        for (ShippedOrderLineItem orderLineItem : this.orderLineItems) {
            orderLineItem.setShippedOrder(this);
        }
    }
}
