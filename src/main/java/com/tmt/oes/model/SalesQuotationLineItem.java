package com.tmt.oes.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.tmt.core.base.BaseSharedObject;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
@EqualsAndHashCode(of = "productCode")
@ToString(exclude = "salesQuotation")
public class SalesQuotationLineItem {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String productCode;
    private String productDescription;
    private BigDecimal listPrice;
    private BigDecimal offeredPrice;
    private Integer quantity;
    private BigDecimal tax;
    private String uom;
    @ManyToOne
    @JoinColumn(name = "sales_quotation_id")
    @JsonBackReference
    private SalesQuotation salesQuotation;
}