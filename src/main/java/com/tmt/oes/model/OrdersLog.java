package com.tmt.oes.model;

import com.tmt.core.base.BaseEntity;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
@ToString
public class OrdersLog implements BaseEntity<Long> {

    @Id
    private Long id;

    private Long lockVersion;

    private String orderNumber;

    private String counterMan;

    private String saleType;

    private String status;

    private String customerCode;

    private Double netAmount;

    private java.util.Date orderCreationDate;

    private java.util.Date orderDate;

    private Double tax;

    private String shippingAddress;

    private String shipZip;

    private java.util.Date printTime;

    private java.util.Date packTime;

    private java.util.Date truckTime;

    private java.util.Date shipTime;

    private String packer;

    private String driver;

    private String deliveryType;

    private java.util.Date shipDate;

    Integer itemsInOrder;

    private Boolean isEmailed;

    private Boolean isPrinted;

}
