package com.tmt.oes.model.enums;

public enum SequenceEntityType {
    SALES_ORDER,
    SALES_QUOTE
}
