package com.tmt.oes.model.enums;

public enum OrderShippingStatus {
    PARTIALLY_SHIPPED,
    FULLY_SHIPPED,
    NOT_SHIPPED
}
