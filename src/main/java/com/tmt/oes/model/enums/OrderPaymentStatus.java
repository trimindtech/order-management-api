package com.tmt.oes.model.enums;

public enum OrderPaymentStatus {
    PARTIALLY_PAID,
    FULLY_PAID,
    UNPAID
}
