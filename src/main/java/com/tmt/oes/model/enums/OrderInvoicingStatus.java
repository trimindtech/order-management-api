package com.tmt.oes.model.enums;

public enum OrderInvoicingStatus {
    PARTIALLY_INVOICED,
    FULLY_INVOICED,
    NOT_INVOICED
}
