package com.tmt.oes.model;

import com.tmt.core.base.BaseReadonlyEntity;
import com.tmt.oes.model.enums.OrderInvoicingStatus;
import com.tmt.oes.model.enums.OrderPaymentStatus;
import com.tmt.oes.model.enums.OrderShippingStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name = "orders")
@EqualsAndHashCode(of = "orderNumber")
public class SalesOrder implements BaseReadonlyEntity<String> {
	@Transient
	private String id;
	@Id
	@Column(name = "ordernumber", nullable = false)
	private String orderNumber;

	@Column(name = "shiptype")
	private String shipType;

	@Column(name = "commissionpercent")
	private Double commissionPercent;

	@Column(name = "credittype")
	private String creditType;

	@Column(name = "customercode")
	private String customerId;

	@Column(name = "customerpo")
	/*@OneToOne
	@NotFound(action = NotFoundAction.IGNORE)
	private CustomerPurchaseOrder customerPurchaseOrder;*/
	private String customerPo;

	@Column(name = "deliverytype")
	private String deliveryType;

	@Column(name = "freightcode")
	private String freightCode;

	@Column(name = "invoicedate")
	@Temporal(value = javax.persistence.TemporalType.TIMESTAMP)
	private Date invoiceDate;

	@Column(name = "misc")
	private String misc;

	@Column(name = "jobsiteid")
	private Long jobsiteId;

	@Column(name = "netamount")
	private Double netAmount;

	@Column(name = "taxrate")
	private Double taxRate;

	@Column(name = "notes")
	private String notes;

	@Column(name = "orderdate")
	private LocalDate orderDate;

	@Column(name = "orderstatus")
	private String orderStatus;

	@Column(name = "pospaid")
	private Long posPaid;

	@Column(name = "pickinglistprntd")
	private String pickinglistPrinted;

	@Column(name = "printonce")
	private String printOnce;

	@Column(name = "resaleno")
	private String resaleNumber;

	@Column(name = "salesman")
	private String salesman;

	@Column(name = "sequencenum")
	private String sequenceNumber;

	@Column(name = "shipaddr1")
	private String shipAddress1;

	@Column(name = "shipaddr2")
	private String shipAddress2;

	@Column(name = "shipaddr3")
	private String shipAddress3;

	@Column(name = "shipcity")
	private String shipCity;

	@Column(name = "shipstate")
	private String shipState;

	@Column(name = "shipphone")
	private String shipPhone;

	@Column(name = "shipdate")
	@Temporal(value = javax.persistence.TemporalType.TIMESTAMP)
	private Date shipDate;

	@Column(name = "shipfrom")
	private String shipFrom;

	@Column(name = "shipmethod")
	private String shipMethod;

	@Column(name = "shipname")
	private String shipName;

	@Column(name = "shipvia")
	private String shipVia;

	@Column(name = "shipzip")
	private String shipZip;

	@Column(name = "soldaddr1")
	private String soldAddress1;

	@Column(name = "soldaddr2")
	private String soldAddress2;

	@Column(name = "soldaddr3")
	private String soldAddress3;

	@Column(name = "soldname")
	private String soldName;

	@Column(name = "tax")
	private Double tax;

	@Column(name = "prevunshipamt")
	private Double previousUnshipAmount;
	@Column(name = "taxpercent")
	private String taxPercent;

	@Column(name = "termnum")
	private String termNumber;

	@Column(name = "terms")
	private String terms;

	@Column(name = "weights")
	private String weights;
	@Column(name = "counterman")
	private String counterman;
	@Column(name = "freight")
	private Double freight;
	@Column(name = "packtime")
	@Temporal(value = javax.persistence.TemporalType.TIMESTAMP)
	private Date packTime;

	@Column(name = "printtime")
	@Temporal(value = javax.persistence.TemporalType.TIMESTAMP)
	private Date printTime;

	@Column(name = "shiptime")
	@Temporal(value = javax.persistence.TemporalType.TIMESTAMP)
	private Date shipTime;

	@Column(name = "status")
	private String status;

	@Column(name = "trucktime")
	@Temporal(value = javax.persistence.TemporalType.TIMESTAMP)
	private Date truckTime;
	@Column(name = "warehouseid")
	private Integer warehouseId;

	@Column(name = "picker")
	private String picker;
	@Column(name = "exempted")
	private Boolean exempted = false;

	@Column(name = "modificationtime")
	@Temporal(value = javax.persistence.TemporalType.TIMESTAMP)
	private Date modificationTime;

	@Column(name = "creationtime")
	@Temporal(value = javax.persistence.TemporalType.TIMESTAMP)
	private Date creationTime;

	@Column(name = "createdby")
	private String createdBy;

	@Column //TODO: Need to add manually in oes db
	@Enumerated(EnumType.STRING)
	private OrderInvoicingStatus invoiceStatus;

	@Column //TODO: Need to add manually in oes db
	@Enumerated(EnumType.STRING)
	private OrderShippingStatus shippingStatus;

	@Column //TODO: Need to add manually in oes db
	@Enumerated(EnumType.STRING)
	private OrderPaymentStatus paymentStatus;

	@Column //TODO: Need to add manually in db
	private BigDecimal amountPaid = BigDecimal.ZERO;

	@OneToMany(mappedBy = "salesOrder")
	private List<SalesOrderLineItem> salesOrderLineItems;

}