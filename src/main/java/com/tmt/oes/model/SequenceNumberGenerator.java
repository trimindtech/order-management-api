package com.tmt.oes.model;

import com.tmt.oes.model.enums.SequenceEntityType;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
public class SequenceNumberGenerator
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long warehouseId;
    private String prefix;
    private Long suffix;
    private Integer length;
    @Enumerated(EnumType.STRING)
    private SequenceEntityType entityType;
}
