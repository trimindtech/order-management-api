package com.tmt.oes.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tmt.core.base.BaseEntity;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;


@Entity
@Data
@EqualsAndHashCode(of = "id")
public class SalesQuotation implements BaseEntity<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;
	Long lockVersion;
	private String quoteNumber;
	private String createdBy;
	private String customerId;
	private String notes;
	private LocalDateTime creationDate;
	@Column(name = "is_sales_order_created")
	private Boolean isSalesOrderCreated;

	@OneToMany(mappedBy = "salesQuotation", cascade = CascadeType.ALL)
	@Setter(AccessLevel.NONE)
	@JsonManagedReference
	private List<SalesQuotationLineItem> salesOrderQuoteLineItems;

	public void setSalesQuotationLineItems(List<SalesQuotationLineItem> orderLineItems) {
		this.salesOrderQuoteLineItems = orderLineItems;
		for (final SalesQuotationLineItem orderLineItem : this.salesOrderQuoteLineItems) {
			orderLineItem.setSalesQuotation(this);
		}
	}
}