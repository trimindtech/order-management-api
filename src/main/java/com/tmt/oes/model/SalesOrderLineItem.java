package com.tmt.oes.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;


@Entity
@Table(name="orderdetails")
@Data
@ToString(exclude = "salesOrder")
@EqualsAndHashCode(of = "itemcode")
public class SalesOrderLineItem {
	
	@Id
	@GeneratedValue(strategy=javax.persistence.GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@Column(name="commission")
	private Double commission;
	
	@Column(name="customercode")
	private String customercode;
	
	@Column(name="invoicenumber")
	private String invoicenumber;
	
	@Column(name="itemcode")
	private String itemcode;
	
	@Column(name="itemdescription")
	private String itemdescription;
	
	@Column(name="itemdiscount")
	private Double itemdiscount;
	
	@Column(name="itemstatus")
	private String itemstatus;
	
	@Column(name="listprice")
	private Double listprice;
	
	@Column(name="note")
	private String note;
	
	@Column(name="orderdiscount")
	private Double orderdiscount;
	
	@Column(name="priceadjustment")
	private Double priceadjustment;
	
	@Column(name="priceentered")
	private Double priceentered;
	
	@Column(name="quantitybackordered")
	private Integer quantitybackordered;
	
	@Column(name="quantityinvoice")
	private Integer quantityinvoice;
	
	@Column(name="quantityordered")
	private Integer quantityordered;
	
	@Column(name="quantityshipped")
	private Integer quantityshipped;
	
	@Column(name="standardprice")
	private Double standardprice;
	
	@Column(name="tax")
	private Double tax;
	
	@Column(name="uom")
	private String uom;
	
	@Column(name="terms")
	private String terms;
	
	@Column(name="deliverytype")
	private String deliveryType;
	
	@Column(name="orderdate")
	@Temporal(value=javax.persistence.TemporalType.TIMESTAMP)
	private java.util.Date orderdate;
	
	@Column(name="quantitypacked")
	private Integer quantitypacked;
	
	@Column(name="packdate")
	@Temporal(value=javax.persistence.TemporalType.DATE)
	private java.util.Date packdate;
	
	@Column(name = "epeligible", nullable = true)
	private Boolean	epeligible;

	@ManyToOne
	@JoinColumn(name = "ordernumber")
	private SalesOrder salesOrder;
}