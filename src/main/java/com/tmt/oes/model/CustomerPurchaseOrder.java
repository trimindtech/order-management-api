package com.tmt.oes.model;

import lombok.Data;

@Data
public class CustomerPurchaseOrder {
    private Long id;
    private String customerId;
    private String salesOrderId;
    private String purchaseOrderId;
}
