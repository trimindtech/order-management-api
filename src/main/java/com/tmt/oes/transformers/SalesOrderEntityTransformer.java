package com.tmt.oes.transformers;

import com.tmt.core.base.EntityToSharedObjectTransformer;
import com.tmt.core.transformer.AbstractCopyPropertiesTransformer;
import com.tmt.oes.model.SalesOrder;
import com.tmt.oes.model.dtos.SalesOrderDTO;
import com.tmt.oes.model.enums.OrderShippingStatus;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Component
public class SalesOrderEntityTransformer extends AbstractCopyPropertiesTransformer<SalesOrder, SalesOrderDTO> implements EntityToSharedObjectTransformer<SalesOrder, SalesOrderDTO, String> {
    @Override
    public SalesOrderDTO apply(SalesOrder salesOrder) {
        final SalesOrderDTO salesOrderDTO = super.apply(salesOrder);
        salesOrderDTO.setTotalAmount(BigDecimal.valueOf(salesOrder.getNetAmount()));
        salesOrderDTO.setTax(BigDecimal.valueOf(salesOrder.getTax()));
        final BigDecimal amountPaid = salesOrder.getAmountPaid();
        salesOrderDTO.setAmountPaid(amountPaid == null ? BigDecimal.ZERO : amountPaid);
        long aging = salesOrderDTO.getOrderDate().until(LocalDate.now(), ChronoUnit.DAYS);
        salesOrderDTO.setAging(aging);
        salesOrderDTO.setInvoiceStatus(salesOrder.getInvoiceStatus().name());
        salesOrderDTO.setPaymentStatus(salesOrder.getPaymentStatus().name());
        final OrderShippingStatus shippingStatus = salesOrder.getShippingStatus();
        salesOrderDTO.setShippingStatus(shippingStatus == null ? OrderShippingStatus.NOT_SHIPPED.name() : shippingStatus.name());
        salesOrderDTO.setSaleType(salesOrder.getCreditType());
        return salesOrderDTO;
    }
}
