package com.tmt.oes.service;

import com.tmt.oes.model.SequenceNumberGenerator;
import com.tmt.oes.model.enums.SequenceEntityType;
import com.tmt.oes.repository.SequenceNumberGeneratorRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.DecimalFormat;
import java.text.NumberFormat;

@Service
@AllArgsConstructor
@Transactional
public class SequenceNumberGeneratorService {

    private final SequenceNumberGeneratorRepository invoiceNumberGeneratorRepository;

    public String generateSequenceNumber(final SequenceEntityType entityType) {
        final SequenceNumberGenerator sequenceNumberGenerator = invoiceNumberGeneratorRepository.findOneByWarehouseIdAndEntityType(1L, entityType);
        int invoiceNumberLength = sequenceNumberGenerator.getLength();
        final String invoiceNumberPrefix = sequenceNumberGenerator.getPrefix();
        Long invoiceNumberSuffix = sequenceNumberGenerator.getSuffix() + 1;
        int possibleSuffixLength = invoiceNumberLength - invoiceNumberPrefix.length();
        final StringBuilder pattern = new StringBuilder();
        for(int i = 0; i < possibleSuffixLength; i++) {
            pattern.append("0");
        }
        NumberFormat formatter = new DecimalFormat(pattern.toString());
        String newSuffixStr = formatter.format(invoiceNumberSuffix);
        final String generatedInvoiceNumber = invoiceNumberPrefix + newSuffixStr;

        sequenceNumberGenerator.setSuffix(invoiceNumberSuffix);
        invoiceNumberGeneratorRepository.save(sequenceNumberGenerator);
        return generatedInvoiceNumber;
    }
}
