package com.tmt.oes.service;

import com.tmt.core.base.AbstractBaseEntityCommandService;
import com.tmt.oes.model.*;
import com.tmt.oes.model.enums.OrderShippingStatus;
import com.tmt.oes.repository.SalesOrderRepository;
import com.tmt.oes.repository.ShippedOrderRepository;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ShippedOrderService extends AbstractBaseEntityCommandService<ShippedOrder, Long> {
    private final SalesOrderRepository salesOrderRepository;
    private final ShippedOrderRepository shippedOrderRepository;

    public ShippedOrderService(ShippedOrderRepository shippedOrderRepository, final SalesOrderRepository salesOrderRepository) {
        super(shippedOrderRepository);
        this.shippedOrderRepository = shippedOrderRepository;
        this.salesOrderRepository = salesOrderRepository;
    }

    @Override
    protected void beforeInsert(ShippedOrder shippedOrder) {
        shippedOrder.setIsInvoiced(false);
    }

    @Override
    protected void afterSave(final ShippedOrder shippedOrder) {
        final SalesOrder salesOrder = salesOrderRepository.findOne(shippedOrder.getSalesOrder().getOrderNumber());
        final OrderShippingStatus orderShippingStatus = determineShippingStatus(salesOrder);
        salesOrder.setShippingStatus(orderShippingStatus);
        salesOrderRepository.save(salesOrder);
    }

    private OrderShippingStatus determineShippingStatus(final SalesOrder salesOrder) {
        final List<SalesOrderLineItem> salesOrderLineItems = salesOrder.getSalesOrderLineItems();
        final List<ShippedOrder> allShippedOrdersOfSalesOrder = shippedOrderRepository.findBySalesOrder(salesOrder);
        Map<String, Integer> shippedItemsCount = allShippedOrdersOfSalesOrder.stream()
                .map(ShippedOrder::getOrderLineItems)
                .flatMap(Collection::stream)
                .collect(Collectors.toList())
                .stream()
                .collect(Collectors.toMap(ShippedOrderLineItem::getProductCode,
                        ShippedOrderLineItem::getQuantityShipped,
                        (item1, item2) -> item1 + item2));

        if(shippedItemsCount.size() == 0) {
            return OrderShippingStatus.NOT_SHIPPED;
        }

        final Map<String, Integer> orderedItemsCount  = salesOrderLineItems.stream()
                .collect(Collectors.toMap(SalesOrderLineItem::getItemcode,
                        SalesOrderLineItem::getQuantityordered));

        if(orderedItemsCount.size() != shippedItemsCount.size()) {
            return OrderShippingStatus.PARTIALLY_SHIPPED;
        }

        boolean noneShipped = true;
        for (final String productCode : orderedItemsCount.keySet()) {
            Integer totalOrdered = orderedItemsCount.getOrDefault(productCode, 0);
            Integer shippedSoFar = shippedItemsCount.getOrDefault(productCode, 0);

            if(shippedSoFar != 0) {
                noneShipped = false;
            }
            if(!Objects.equals(totalOrdered, shippedSoFar) && shippedSoFar != 0) {
                return OrderShippingStatus.PARTIALLY_SHIPPED;
            }
        }
        return noneShipped ? OrderShippingStatus.NOT_SHIPPED : OrderShippingStatus.FULLY_SHIPPED;
    }
}
