package com.tmt.oes.service;

import com.tmt.oes.exception.ResourceNotFoundException;
import com.tmt.oes.model.SalesOrder;
import com.tmt.oes.model.SalesOrderLineItem;
import com.tmt.oes.model.ShippedOrder;
import com.tmt.oes.model.ShippedOrderLineItem;
import com.tmt.oes.model.dtos.InvoiceDTO;
import com.tmt.oes.model.dtos.InvoiceItemDTO;
import com.tmt.oes.repository.SalesOrderRepository;
import com.tmt.oes.repository.ShippedOrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class InvoicePreviewService {

    private final ShippedOrderRepository shippedOrderRepository;
    private final SalesOrderRepository salesOrderRepository;

    public List<InvoiceDTO> generatePreviewForCustomer(final String customerId, final LocalDate custOffDate, final String userId) {
        final List<ShippedOrder> uninvoicedShippedOrders = shippedOrderRepository.findBySalesOrderCustomerIdAndIsInvoicedAndShippedDateLessThanEqual(customerId, false, custOffDate);
        return generateInvoicesPreview(uninvoicedShippedOrders, userId);

    }

    public List<InvoiceDTO> generatePreviewForOrdersShippedBeforeDate(final LocalDate cutOffDate, final String userId) {
        final List<ShippedOrder> uninvoicedShippedOrders = shippedOrderRepository.findByShippedDateLessThanEqualAndIsInvoiced(cutOffDate, false);
        return generateInvoicesPreview(uninvoicedShippedOrders, userId);
    }

    private List<InvoiceDTO> generateInvoicesPreview(final List<ShippedOrder> uninvoicedShippedOrders, final String userId) {
        final List<InvoiceDTO> invoiceDTOList = new ArrayList<>();

        for (final ShippedOrder shippedOrder : uninvoicedShippedOrders) {
            final SalesOrder salesOrder = shippedOrder.getSalesOrder();
            final List<SalesOrderLineItem> orderLineItems = salesOrder.getSalesOrderLineItems();
            final BigDecimal taxPercentage = BigDecimal.valueOf(salesOrder.getTaxRate());

            final Map<String, SalesOrderLineItem> orderLineItemMap = orderLineItems.stream().collect(Collectors.toMap(SalesOrderLineItem::getItemcode, orderLineItem -> orderLineItem));

            final InvoiceDTO invoiceDTO = new InvoiceDTO();
            invoiceDTO.setCustomerId(salesOrder.getCustomerId());
            invoiceDTO.setInvoiceDate(LocalDate.now());
            invoiceDTO.setShipmentId(shippedOrder.getId());

            BigDecimal invoiceAmount = BigDecimal.ZERO;

            final List<InvoiceItemDTO> invoiceItemDTOList = new ArrayList<>();
            for (final ShippedOrderLineItem shippedOrderLineItem : shippedOrder.getOrderLineItems()) {
                final SalesOrderLineItem orderLineItem = orderLineItemMap.get(shippedOrderLineItem.getProductCode());
                InvoiceItemDTO invoiceItemDTO = new InvoiceItemDTO();
                invoiceItemDTO.setProductCode(shippedOrderLineItem.getProductCode());
                Integer quantityShipped = shippedOrderLineItem.getQuantityShipped();
                invoiceItemDTO.setQuantity(quantityShipped);
                invoiceItemDTO.setPrice(BigDecimal.valueOf(orderLineItem.getPriceentered()));
                final BigDecimal taxOnItem = BigDecimal.valueOf(orderLineItem.getPriceentered()).multiply(taxPercentage.divide(new BigDecimal(100)));
                invoiceItemDTO.setTaxAmount(taxOnItem);

                invoiceAmount = invoiceAmount.add(BigDecimal.valueOf(orderLineItem.getPriceentered())
                        .multiply(BigDecimal.valueOf(quantityShipped)));

                invoiceItemDTOList.add(invoiceItemDTO);
            }

            invoiceDTO.setInvoiceAmount(invoiceAmount);
            invoiceDTO.setDiscount(BigDecimal.ZERO); //TODO: Define how the disount is going to be applied and then calculate here
            invoiceDTO.setFreightCharges(shippedOrder.getFreightCharges());
            invoiceDTO.setMiscellaneousCharges(BigDecimal.ZERO); //TODO: Get clarification from Nachman
            invoiceDTO.setOrderNumber(salesOrder.getOrderNumber());
            final BigDecimal taxOnInvoice = invoiceAmount.multiply(taxPercentage.divide(new BigDecimal(100)));
            invoiceDTO.setTaxAmount(taxOnInvoice);

            invoiceDTO.setInvoiceItemDTOS(invoiceItemDTOList);
            invoiceDTOList.add(invoiceDTO);
        }
        return invoiceDTOList;
    }

    public List<InvoiceDTO> generatePreviewForOrder(String orderId, String userId) {
        SalesOrder salesOrder = salesOrderRepository.findOne(orderId);
        if(salesOrder == null) {
            throw new ResourceNotFoundException("No SalesOrder found with given Id: " + orderId);
        }
        final List<ShippedOrder> uninvoicedShippedOrders = shippedOrderRepository.findBySalesOrderAndIsInvoiced(salesOrder, false);
        return generateInvoicesPreview(uninvoicedShippedOrders, userId);
    }
}
