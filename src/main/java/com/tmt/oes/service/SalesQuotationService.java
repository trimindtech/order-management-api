package com.tmt.oes.service;

import com.tmt.core.base.AbstractBaseEntityCommandService;
import com.tmt.oes.model.SalesQuotation;
import com.tmt.oes.model.enums.SequenceEntityType;
import com.tmt.oes.repository.SalesQuotationRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class SalesQuotationService extends AbstractBaseEntityCommandService<SalesQuotation, Long> {
    private final SequenceNumberGeneratorService sequenceNumberGeneratorService;

    public SalesQuotationService(final SalesQuotationRepository repository, SequenceNumberGeneratorService sequenceNumberGeneratorService) {
        super(repository);
        this.sequenceNumberGeneratorService = sequenceNumberGeneratorService;
    }

    @Override
    protected void beforeInsert(final SalesQuotation salesQuotation) {
        String quoteNumber = sequenceNumberGeneratorService.generateSequenceNumber(SequenceEntityType.SALES_QUOTE);
        if(salesQuotation.getCreationDate() == null) {
            salesQuotation.setCreationDate(LocalDateTime.now());
        }
        if(salesQuotation.getIsSalesOrderCreated() == null) {
            salesQuotation.setIsSalesOrderCreated(false);
        }
        salesQuotation.setQuoteNumber(quoteNumber);
    }
}
