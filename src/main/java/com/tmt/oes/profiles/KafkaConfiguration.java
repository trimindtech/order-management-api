package com.tmt.oes.profiles;

import java.util.Map;

public interface KafkaConfiguration {
    Map<String, Object> producerConfiguration();
    Map<String, Object> consumerConfiguration();
    default String getTopicPrefix() {
        return "";
    }
}
