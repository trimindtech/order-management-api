package com.tmt.oes.profiles;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("!shared-kafka")
public @interface DefaultKafkaProfile {


}
