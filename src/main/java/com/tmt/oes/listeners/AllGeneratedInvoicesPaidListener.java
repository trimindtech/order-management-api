package com.tmt.oes.listeners;

import com.tmt.oes.invoice.model.sharedobjects.AllGeneratedInvoicesOfOrderPaidEvent;
import com.tmt.oes.model.SalesOrder;
import com.tmt.oes.model.enums.OrderInvoicingStatus;
import com.tmt.oes.model.enums.OrderPaymentStatus;
import com.tmt.oes.repository.SalesOrderRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Slf4j
@Component
@AllArgsConstructor
@Transactional
public class AllGeneratedInvoicesPaidListener {

    private final SalesOrderRepository salesOrderRepository;

    @KafkaListener(topics = "${kafka.custom.topic.prefix}oes-all-invoices-created-topic", containerFactory = "allInvoiceOfOrderPaidListenerContainerFactory", group = "invoice-created")
    public void receive(final AllGeneratedInvoicesOfOrderPaidEvent invoicesOfOrderPaidEvent) {
        log.info("AllGeneratedInvoicesOfOrderPaidEvent = '{}'", invoicesOfOrderPaidEvent);

        final SalesOrder salesOrder = salesOrderRepository.getOne(invoicesOfOrderPaidEvent.getOrderNumber());
        if(salesOrder != null && salesOrder.getPaymentStatus() != OrderPaymentStatus.FULLY_PAID) {
            salesOrder.setPaymentStatus(salesOrder.getInvoiceStatus() == OrderInvoicingStatus.FULLY_INVOICED ?
                    OrderPaymentStatus.FULLY_PAID : OrderPaymentStatus.PARTIALLY_PAID);
            salesOrderRepository.save(salesOrder);
            log.info("Updated payment status of sales-order " + salesOrder.getOrderNumber());
        }
        log.info("Processed AllGeneratedInvoicesOfOrderPaidEvent " + invoicesOfOrderPaidEvent.getEventId());
    }
}