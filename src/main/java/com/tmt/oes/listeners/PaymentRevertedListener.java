package com.tmt.oes.listeners;

import com.tmt.oes.invoice.model.sharedobjects.PaymentRevertedEvent;
import com.tmt.oes.model.SalesOrder;
import com.tmt.oes.model.enums.OrderPaymentStatus;
import com.tmt.oes.repository.SalesOrderRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Slf4j
@Component
@AllArgsConstructor
public class PaymentRevertedListener {

    private SalesOrderRepository salesOrderRepository;

    @KafkaListener(topics = "${kafka.custom.topic.prefix}oes-topic", containerFactory = "paymentRevertedEventListenerContainerFactory", group = "orders-reverted-payments")
    public void receive(final PaymentRevertedEvent paymentRevertedEvent) {
        log.info("PaymentRevertedEvent = '{}'", paymentRevertedEvent);
        final SalesOrder salesOrder = salesOrderRepository.findOne(paymentRevertedEvent.getInvoiceNumber());
        if(salesOrder != null) {
            final BigDecimal currentAmountPaid = salesOrder.getAmountPaid().subtract(paymentRevertedEvent.getAmount());
            salesOrder.setAmountPaid(currentAmountPaid);
            if(currentAmountPaid.compareTo(BigDecimal.ZERO) == 0) {
                salesOrder.setPaymentStatus(OrderPaymentStatus.UNPAID);
            }
            salesOrderRepository.save(salesOrder);

            log.info("Processed PaymentRevertedEvent " + paymentRevertedEvent.getEventId());
        }
    }
}
