package com.tmt.oes.listeners;

import com.tmt.oes.invoice.model.sharedobjects.InvoiceCreatedEvent;
import com.tmt.oes.model.SalesOrder;
import com.tmt.oes.model.SalesOrderLineItem;
import com.tmt.oes.model.ShippedOrder;
import com.tmt.oes.model.ShippedOrderLineItem;
import com.tmt.oes.model.enums.OrderInvoicingStatus;
import com.tmt.oes.model.enums.OrderShippingStatus;
import com.tmt.oes.repository.SalesOrderRepository;
import com.tmt.oes.repository.ShippedOrderRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Component
@AllArgsConstructor
@Transactional
public class InvoiceCreatedListener {

    private final SalesOrderRepository salesOrderRepository;
    private final ShippedOrderRepository shippedOrderRepository;

    @KafkaListener(topics = "${kafka.custom.topic.prefix}oes-invoice-topic", containerFactory = "invoiceCreatedListenerContainerFactory", group = "invoice-created")
    public void receive(final InvoiceCreatedEvent invoiceCreatedEvent) {
        log.info("InvoiceCreatedEvent = '{}'", invoiceCreatedEvent);

        final ShippedOrder shippedOrder = shippedOrderRepository.findOne(invoiceCreatedEvent.getShipmentId());
        Map<String, Integer> itemCountInvoicedMap = new HashMap<>();
        if(shippedOrder != null) {
            shippedOrder.setIsInvoiced(true);
            shippedOrderRepository.save(shippedOrder);
            itemCountInvoicedMap = shippedOrder.getOrderLineItems().stream().collect(Collectors.toMap(ShippedOrderLineItem::getProductCode, ShippedOrderLineItem::getQuantityShipped));
            for (ShippedOrderLineItem shippedOrderLineItem : shippedOrder.getOrderLineItems()) {
                itemCountInvoicedMap.put(shippedOrderLineItem.getProductCode(), shippedOrderLineItem.getQuantityShipped());
            }
        }


        final SalesOrder salesOrder = salesOrderRepository.getOne(invoiceCreatedEvent.getOrderNumber());
        if(salesOrder != null) {
            salesOrder.setInvoiceStatus(determineInvoicingStatus(salesOrder));
            for (SalesOrderLineItem salesOrderLineItem : salesOrder.getSalesOrderLineItems()) {
                final String productCode = salesOrderLineItem.getItemcode();
                Integer invoicedCount = itemCountInvoicedMap.get(productCode);
                if(invoicedCount != null) {
                    Integer existingInvoiced = salesOrderLineItem.getQuantityinvoice();
                    if(existingInvoiced == null) existingInvoiced = 0;
                    salesOrderLineItem.setQuantityinvoice(existingInvoiced + invoicedCount);
                }
            }
            salesOrderRepository.save(salesOrder);
            log.info("Updated invoicing status of sales-order " + salesOrder.getOrderNumber());
        }
        log.info("Processed InvoiceCreatedEvent " + invoiceCreatedEvent.getEventId());
    }

    private OrderInvoicingStatus determineInvoicingStatus(final SalesOrder salesOrder) {
        if(salesOrder.getShippingStatus() == OrderShippingStatus.NOT_SHIPPED) {
            //An order cannot be invoiced unless at least some part of it is shipped
            return OrderInvoicingStatus.NOT_INVOICED;
        }
        final List<ShippedOrder> allShippedOrders = shippedOrderRepository.findBySalesOrder(salesOrder);
        boolean areAllShippedOrdersInvoiced = allShippedOrders.stream().filter(shippedOrder -> !shippedOrder.getIsInvoiced()).count() == 0;
        return areAllShippedOrdersInvoiced && salesOrder.getShippingStatus() == OrderShippingStatus.FULLY_SHIPPED ? OrderInvoicingStatus.FULLY_INVOICED
                : OrderInvoicingStatus.PARTIALLY_INVOICED;
    }
}