package com.tmt.oes.listeners;

import com.tmt.oes.invoice.model.sharedobjects.PaymentReceivedEvent;
import com.tmt.oes.model.SalesOrder;
import com.tmt.oes.model.enums.OrderPaymentStatus;
import com.tmt.oes.repository.SalesOrderRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Slf4j
@Component
@AllArgsConstructor
public class PaymentReceivedListener {

    private final SalesOrderRepository salesOrderRepository;

    @KafkaListener(topics = "${kafka.custom.topic.prefix}oes-topic", containerFactory = "paymentReceivedEventListenerContainerFactory", group = "orders-payments")
    public void receive(final PaymentReceivedEvent paymentReceivedEvent) {
        log.info("PaymentReceivedEvent = '{}'", paymentReceivedEvent);
        final SalesOrder salesOrder = salesOrderRepository.findOne(paymentReceivedEvent.getOrderNumber());
        if(salesOrder != null) {
            BigDecimal existingAmountPaid = salesOrder.getAmountPaid();
            if(existingAmountPaid == null) {
                existingAmountPaid = BigDecimal.ZERO;
            }
            final BigDecimal currentAmountPaid = existingAmountPaid.add(paymentReceivedEvent.getAmount());
            salesOrder.setAmountPaid(currentAmountPaid);

            final OrderPaymentStatus orderPaymentStatus = new BigDecimal(salesOrder.getNetAmount() + salesOrder.getTax()).setScale(2, BigDecimal.ROUND_HALF_EVEN)
                    .compareTo(currentAmountPaid) <= 0 ? OrderPaymentStatus.FULLY_PAID : OrderPaymentStatus.PARTIALLY_PAID;
            salesOrder.setPaymentStatus(orderPaymentStatus);
            salesOrderRepository.save(salesOrder);
            log.info("Processed PaymentReceivedEvent " + paymentReceivedEvent.getEventId());
        }
    }
}
