package com.tmt.oes.config;

import com.tmt.oes.invoice.model.sharedobjects.AllGeneratedInvoicesOfOrderPaidEvent;
import com.tmt.oes.invoice.model.sharedobjects.InvoiceCreatedEvent;
import com.tmt.oes.invoice.model.sharedobjects.PaymentReceivedEvent;
import com.tmt.oes.invoice.model.sharedobjects.PaymentRevertedEvent;
import com.tmt.oes.profiles.KafkaConfiguration;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.Map;

@Configuration
@EnableKafka
@AllArgsConstructor
public class KafkaReceiverConfig {

    private final KafkaConfiguration kafkaConfiguration;

    @Bean
    public ConsumerFactory<String, String> consumerFactory() {
        return new DefaultKafkaConsumerFactory<>(kafkaConfiguration.consumerConfiguration(), new StringDeserializer(),
                new JsonDeserializer<>(String.class));
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, String> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, String> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());

        return factory;
    }

    @Bean
    public ConsumerFactory<String, InvoiceCreatedEvent> invoiceCreatedConsumerFactory() {
        Map<String, Object> props = kafkaConfiguration.consumerConfiguration();
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "invoice-created");
        return new DefaultKafkaConsumerFactory<>(props, new StringDeserializer(),
                new JsonDeserializer<>(InvoiceCreatedEvent.class));
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, InvoiceCreatedEvent> invoiceCreatedListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, InvoiceCreatedEvent> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(invoiceCreatedConsumerFactory());
        factory.setRecordFilterStrategy(consumerRecord -> consumerRecord.value() == null || StringUtils.isAnyEmpty(consumerRecord.value().getOrderNumber(), consumerRecord.value().getInvoiceNumber())
                                                    || consumerRecord.value().getShipmentId() == null || consumerRecord.value().getShipmentId() == 0);
        return factory;
    }

    @Bean
    public ConsumerFactory<String, AllGeneratedInvoicesOfOrderPaidEvent> allInvoicesOfOrderPaidConsumerFactory() {
        Map<String, Object> props = kafkaConfiguration.consumerConfiguration();
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "all-invoices-paid");
        return new DefaultKafkaConsumerFactory<>(props, new StringDeserializer(),
                new JsonDeserializer<>(AllGeneratedInvoicesOfOrderPaidEvent.class));
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, AllGeneratedInvoicesOfOrderPaidEvent> allInvoiceOfOrderPaidListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, AllGeneratedInvoicesOfOrderPaidEvent> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(allInvoicesOfOrderPaidConsumerFactory());
        factory.setRecordFilterStrategy(consumerRecord -> consumerRecord.value() == null || StringUtils.isEmpty(consumerRecord.value().getOrderNumber()));
        return factory;
    }

    @Bean
    public ConsumerFactory<String, PaymentReceivedEvent> paymentReceivedEventConsumerFactory() {
        final Map<String, Object> props = kafkaConfiguration.consumerConfiguration();
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "payments");
        return new DefaultKafkaConsumerFactory<>(props, new StringDeserializer(),
                new JsonDeserializer<>(PaymentReceivedEvent.class));
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, PaymentReceivedEvent> paymentReceivedEventListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, PaymentReceivedEvent> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(paymentReceivedEventConsumerFactory());
        factory.setRecordFilterStrategy(consumerRecord -> consumerRecord.value() == null
                || consumerRecord.value().getDatePaid() == null
                || StringUtils.isBlank(consumerRecord.value().getOrderNumber())
                || consumerRecord.value().getAmount() == null);
        return factory;
    }

    @Bean
    public ConsumerFactory<String, PaymentRevertedEvent> paymentRevertedEventConsumerFactory() {
        final Map<String, Object> props = kafkaConfiguration.consumerConfiguration();
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "reverted-payments");
        return new DefaultKafkaConsumerFactory<>(props, new StringDeserializer(),
                new JsonDeserializer<>(PaymentRevertedEvent.class));
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, PaymentRevertedEvent> paymentRevertedEventListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, PaymentRevertedEvent> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(paymentRevertedEventConsumerFactory());
        factory.setRecordFilterStrategy(consumerRecord -> consumerRecord.value() == null
                || consumerRecord.value().getAmount() == null
                || consumerRecord.value().getDateReverted() == null);
        return factory;
    }
}
