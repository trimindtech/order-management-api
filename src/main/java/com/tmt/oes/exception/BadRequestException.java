package com.tmt.oes.exception;

public class BadRequestException extends Exception {
    private String resourceId;

    public BadRequestException(String resourceId, String message) {
        super(message);
        this.resourceId = resourceId;
    }

}
