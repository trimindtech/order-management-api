package com.tmt.oes.exception;

import lombok.*;
import java.util.List;

@Data
@NoArgsConstructor
class ApiError {
    private String errorCode;
    private String errorMessage;
    private List<String> errors;

}