package com.tmt.oes.controller;

import com.tmt.oes.model.OrdersLog;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

//@RunWith(SpringRunner.class)
//@SpringBootTest
//@AutoConfigureMockMvc
public class OrdersLogControllerTest {

//    @Autowired
    private MockMvc mvc;

//    @Mock
    private Page<OrdersLog> ordersLog;


    /*@Test
    public void testGetOrdersReturnsOk() throws Exception {
        when(ordersLogService.findOrders(anyObject(), anyObject())).thenReturn(ordersLog);
        mvc.perform(MockMvcRequestBuilders.get("/orders").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void testGetOrdersReturnsNotFound() throws Exception {
        when(ordersLogService.findOrders(anyObject(), anyObject())).thenReturn(ordersLog);
        mvc.perform(MockMvcRequestBuilders.get("/orders/1234").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetOrdersWithProperOrderDateReturnsOk() throws Exception {
        when(ordersLogService.findOrders(anyObject(), anyObject())).thenReturn(ordersLog);
        mvc.perform(MockMvcRequestBuilders.get("/orders")
                .param("orderDate", "2017-07-30")
                .param("orderDate", "2017-08-30")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void testGetOrdersWithImproperProperOrderDateReturnsBadRequest() throws Exception {
        when(ordersLogService.findOrders(anyObject(), anyObject())).thenReturn(ordersLog);
        mvc.perform(MockMvcRequestBuilders.get("/orders")
                .param("orderDate", "1234")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }*/
}