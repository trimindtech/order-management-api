package com.tmt.oes.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URL;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

//@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class OrdersLogControllerIT {

//    @LocalServerPort
    private int port;

    private URL base;

//    @Autowired
    private TestRestTemplate template;

//    @Before
    public void setUp() throws Exception {
        this.base = new URL("http://localhost:" + port + "/");
    }

//    @Test
    public void getOrdersReturnsOk() throws Exception {
        ResponseEntity<String> response = template.getForEntity(base.toString() + "orders",
                String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

//    @Test
    public void getOrdersReturnsBadRequest() throws Exception {
        ResponseEntity<String> response = template.getForEntity(base.toString() + "orders?order_date=123",
                String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

//    @Test
    public void getOrdersReturnsNotFound() throws Exception {
        ResponseEntity<String> response = template.getForEntity(base.toString(),
                String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
    }
}
