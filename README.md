**Welcome to Order Management API**

1. This api is going to serve as the back-end for orders log operation at the moment.
2. This is a spring boot application.
3. The DB Migrations are handled using Flyway DB Migration Tool
4. The backend for this application is mysql
5. To run the application you need to do
`mvn clean install` following running the `OrderManagementApplication` class
6. To access the swagger ui set up please run the application locally and access it using:
`http://localhost:8080/swagger-ui.html
`


Happy coding!!

Team TrimindTech
